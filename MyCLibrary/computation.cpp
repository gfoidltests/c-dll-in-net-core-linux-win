#include "computation.h"

int add_int(const int a, const int b)
{
    return a + b;
}

double add_double(const double a, const double b)
{
    return a + b;
}