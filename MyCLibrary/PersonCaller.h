#pragma once

#include "Dll.h"
#include "Person.h"

#ifdef __cplusplus
extern "C"
{
#endif

    EXPORT void* person_create(const char* name);
    EXPORT void person_dispose(void* person);
    EXPORT void person_info(void* person);
    EXPORT void person_love(void* person, void* other);

#ifdef __cplusplus
}
#endif