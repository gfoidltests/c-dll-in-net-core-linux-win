#pragma once


#include "Dll.h"

#ifdef __cplusplus
extern "C"
{
#endif

    EXPORT int add_int(const int a, const int b);
    EXPORT double add_double(const double a, const double b);

#ifdef __cplusplus
}
#endif