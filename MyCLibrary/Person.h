#pragma once

#include <string>

class Person
{
public:
    Person(const std::string& name);
    void Info();
    void Love(const Person& other);

private:
    std::string _name;
};