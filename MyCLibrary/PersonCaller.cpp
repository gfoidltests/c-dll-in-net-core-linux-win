#include <string>
#include "PersonCaller.h"

void* person_create(const char* name)
{
    using std::string;

    string tmp = string(name);
    return new Person(tmp);
}

void person_dispose(void* person)
{
    if (person == NULL) return;

    delete person;
    person = NULL;
}

void person_info(void* person)
{
    if (person == NULL) return;

    ((Person*)person)->Info();
}

void person_love(void* person, void* other)
{
    if (person == NULL || other == NULL) return;

    ((Person*)person)->Love(*(Person*)other);
}