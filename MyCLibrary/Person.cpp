#include <iostream>
#include "Person.h"

using namespace std;

Person::Person(const string& name) : _name(name)
{}

void Person::Info()
{
    cout << "Person: " << _name << endl;
}

void Person::Love(const Person& other)
{
    cout << _name << " loves " << other._name << endl;
}