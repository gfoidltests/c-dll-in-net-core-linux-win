#!/bin/bash

runCore() {
    echo running $1
    echo
    dotnet $1.dll
    echo
}

runCore MyCHostnet
runCore MyCHostnetLoad
