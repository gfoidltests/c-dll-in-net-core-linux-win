#include "vector.h"

int main()
{
    void* vec1 = vector_create(3);
    vector_index_set(vec1, 0, 1);
    vector_index_set(vec1, 1, 3);
    vector_index_set(vec1, 2, 2);

    vector_destroy(vec1);

    double* arr = new double[3]{ 1, 3, 2 };
    void* vec2 = vector_create_with_array(arr, 3);
    double val = vector_index_get(vec2, 1);

    vector_destroy(vec2);
}