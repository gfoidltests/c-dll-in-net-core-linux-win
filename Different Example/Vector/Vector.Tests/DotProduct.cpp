#include "stdafx.h"
#include "Vector.h"

namespace VectorTests
{
    TEST_CLASS(DotProduct)
    {
    public:
        TEST_METHOD(Vectors_given___correct_result)
        {
            double* arr1 = new double[3]{ 1, 2, 3 };
            double* arr2 = new double[3]{ 4, 5, 6 };
            Vector vec1(arr1, 3);
            Vector vec2(arr2, 3);

            double actual = vec1.DotProduct(vec2);

            Assert::AreEqual(32, actual, 1e-6);
        }
        //---------------------------------------------------------------------
        TEST_METHOD(Vector_sizes_do_not_match___Throws_Index_Exception)
        {
            Vector v1(3);
            Vector v2(2);
            bool exceptionThrown = false;

            try
            {
                double actual = v1.DotProduct(v2);
            }
            catch (std::invalid_argument& ex)
            {
                exceptionThrown = true;
            }

            Assert::IsTrue(exceptionThrown);
        }
    };
}