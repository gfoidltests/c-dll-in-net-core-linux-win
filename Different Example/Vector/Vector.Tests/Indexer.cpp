#include "stdafx.h"
#include "Vector.h"

namespace VectorTests
{
    TEST_CLASS(Indexer)
    {
    public:
        TEST_METHOD(Index_smaller_than_size___correct_element)
        {
            double* arr = new double[3]{ 1, 2, 3 };
            Vector sut(arr, 3);

            double actual = sut[1];

            Assert::AreEqual(2, actual, 1e-6);
        }
        //---------------------------------------------------------------------
        TEST_METHOD(Index_greater_than_size___Throws_Index_Exception)
        {
            Vector sut(3);
            bool exceptionThrown = false;

            try
            {
                double actual = sut[3];
            }
            catch (std::invalid_argument& ex)
            {
                exceptionThrown = true;
            }

            Assert::IsTrue(exceptionThrown);
        }
    };
}