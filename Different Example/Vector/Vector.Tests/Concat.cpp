#include "stdafx.h"
#include "Vector.h"

namespace VectorTests
{
    TEST_CLASS(Concat)
    {
    public:
        TEST_METHOD(Vectors_given___correct_resulting_vector)
        {
            double* arr1 = new double[3] {1, 2, 3};
            double* arr2 = new double[2] {4, 5};
            Vector vec1(arr1, 3);
            Vector vec2(arr2, 2);

            Vector* actual = Vector::Concat(vec1, vec2);

            Assert::AreEqual((uint)5, actual->getSize());
            Assert::AreEqual(1, (*actual)[0], 1e-6);
            Assert::AreEqual(2, (*actual)[1], 1e-6);
            Assert::AreEqual(3, (*actual)[2], 1e-6);
            Assert::AreEqual(4, (*actual)[3], 1e-6);
            Assert::AreEqual(5, (*actual)[4], 1e-6);
        }
    };
}