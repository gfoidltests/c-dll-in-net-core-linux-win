#include "stdafx.h"
#include "Vector.h"

namespace VectorTests
{
    TEST_CLASS(Mask)
    {
    public:
        TEST_METHOD(Mask_given___correct_Vector)
        {
            double* arr = new double[3]{ 1, 2, 3 };
            Vector sut(arr, 3);
            bool* mask = new bool[3]{ true, false, true };

            sut.Mask(mask);

            Assert::AreEqual(0, sut[0], 1e-6);
            Assert::AreEqual(2, sut[1], 1e-6);
            Assert::AreEqual(0, sut[2], 1e-6);
        }
    };
}