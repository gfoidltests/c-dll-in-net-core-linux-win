#include "stdafx.h"
#include "Vector.h"

namespace VectorTests
{
    TEST_CLASS(Ones)
    {
    public:
        TEST_METHOD(Size_given___array_with_1_values)
        {
            Vector* sut = Vector::Ones(3);

            for (uint i = 0; i < 3; ++i)
                Assert::AreEqual(1, (*sut)[i], 1e-6);

            delete sut;
        }
    };
    //-------------------------------------------------------------------------
    TEST_CLASS(Zeros)
    {
    public:
        TEST_METHOD(Size_given___array_with_0_values)
        {
            Vector* sut = Vector::Zeros(3);

            for (uint i = 0; i < 3; ++i)
                Assert::AreEqual(0, (*sut)[i], 1e-6);

            delete sut;
        }
    };
}