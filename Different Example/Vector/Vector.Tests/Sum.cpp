#include "stdafx.h"
#include "Vector.h"

namespace VectorTests
{
    TEST_CLASS(Sum)
    {
    public:
        TEST_METHOD(Correct_Sum)
        {
            double* arr = new double[3]{ 1, 2, 3 };
            Vector sut(arr, 3);

            double actual = sut.Sum();

            Assert::AreEqual(6, actual, 1e-6);
        }
    };
}