#include "stdafx.h"
#include "Vector.h"

namespace VectorTests
{
    TEST_CLASS(IsOrthogonalTo)
    {
    public:
        TEST_METHOD(Orthogonal_vectors_given___true)
        {
            double* arr1 = new double[2]{ 2, 1 };
            double* arr2 = new double[2]{ 1, -2 };
            Vector v1(arr1, 2);
            Vector v2(arr2, 2);

            bool actual = v1.IsOrthogonalTo(v2);

            Assert::IsTrue(actual);
        }
        //---------------------------------------------------------------------
        TEST_METHOD(Vectors_given___false)
        {
            double* arr1 = new double[2]{ 2, 1 };
            double* arr2 = new double[2]{ 1, 2 };
            Vector v1(arr1, 2);
            Vector v2(arr2, 2);

            bool actual = v1.IsOrthogonalTo(v2);

            Assert::IsFalse(actual);
        }
        //---------------------------------------------------------------------
        TEST_METHOD(Vector_sizes_do_not_match___Throws_Index_Exception)
        {
            Vector v1(3);
            Vector v2(2);
            bool exceptionThrown = false;

            try
            {
                bool actual = v1.IsOrthogonalTo(v2);
            }
            catch (std::invalid_argument& ex)
            {
                exceptionThrown = true;
            }

            Assert::IsTrue(exceptionThrown);
        }
    };
}