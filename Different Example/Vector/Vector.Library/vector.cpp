#include "vector.h"
//-----------------------------------------------------------------------------
void* vector_create(const uint size)
{
    return new Vector(size);
}
//-----------------------------------------------------------------------------
void* vector_create_with_array(double* arr, const uint size)
{
    return new Vector(arr, size);
}
//-----------------------------------------------------------------------------
void vector_destroy(void* vector)
{
    Vector* vec = (Vector*)vector;

    delete vec;
}
//-----------------------------------------------------------------------------
uint vector_get_size(void* vector)
{
    Vector* vec = (Vector*)vector;

    return vec->getSize();
}
//-----------------------------------------------------------------------------
bool vector_is_array_extern(void* vector)
{
    Vector* vec = (Vector*)vector;

    return vec->getIsArrayExtern();
}
//-----------------------------------------------------------------------------
double vector_index_get(void* vector, const uint i)
{
    Vector* vec = (Vector*)vector;

    return (*vec)[i];
}
//-----------------------------------------------------------------------------
void vector_index_set(void* vector, const uint i, double value)
{
    /*
        Vector vec = *(Vector*)vector
        ist zwar m�glich, aber dann ist das Vector-Objekt am Stack 
        und wird bei Scope-Ende per Destructor gel�scht.
        Das ist eine Fall in die ich getappt bin.
        Daher nur bei Bedarf dereferenzieren.
    */
    Vector* vec = (Vector*)vector;

    (*vec)[i] = value;
}
//-----------------------------------------------------------------------------
double vector_dotproduct(void* vec1, void* vec2)
{
    Vector* v1 = (Vector*)vec1;
    Vector* v2 = (Vector*)vec2;

    return v1->DotProduct(*v2);
}
//-----------------------------------------------------------------------------
double vector_sum(void* vector)
{
    Vector* vec = (Vector*)vector;

    return vec->Sum();
}
//-----------------------------------------------------------------------------
bool vector_is_orthogonalto(void* vec1, void* vec2)
{
    Vector* v1 = (Vector*)vec1;
    Vector* v2 = (Vector*)vec2;

    return v1->IsOrthogonalTo(*v2);
}
//-----------------------------------------------------------------------------
void vector_add_scalar(void* vector, const double value)
{
    Vector* vec = (Vector*)vector;

    vec->Add(value);
}
//-----------------------------------------------------------------------------
void vector_add_vector(void* vector, const Vector& other)
{
    Vector* vec = (Vector*)vector;

    vec->Add(other);
}
//-----------------------------------------------------------------------------
void vector_mask(void* vector, const bool* mask)
{
    Vector* vec = (Vector*)vector;

    vec->Mask(mask);
}
//-----------------------------------------------------------------------------
void* vector_create_zeros(const uint size)
{
    return Vector::Zeros(size);
}
//-----------------------------------------------------------------------------
void* vector_create_ones(const uint size)
{
    return Vector::Ones(size);
}
//-----------------------------------------------------------------------------
void* vector_concat(const void* first, const void* second)
{
    Vector* v1 = (Vector*)first;
    Vector* v2 = (Vector*)second;

    return Vector::Concat(*v1, *v2);
}