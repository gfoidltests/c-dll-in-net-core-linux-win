#pragma once

#include "Dll.h"
#include "../Vector/Vector.h"

#ifdef __cplusplus
extern "C"
{
#endif
    EXPORT void* vector_create(const uint size);
    EXPORT void* vector_create_with_array(double* arr, const uint size);
    EXPORT void vector_destroy(void* vector);

    EXPORT uint vector_get_size(void* vector);
    EXPORT bool vector_is_array_extern(void* vector);

    EXPORT double vector_index_get(void* vector, const uint i);
    EXPORT void vector_index_set(void* vector, const uint i, double value);
    
    EXPORT double vector_dotproduct(void* vec1, void* vec2);
    EXPORT double vector_sum(void* vector);
    EXPORT bool vector_is_orthogonalto(void* vec1, void* vec2);

    EXPORT void vector_add_scalar(void* vector, const double value);
    EXPORT void vector_add_vector(void* vector, const Vector& other);
    EXPORT void vector_mask(void* vector, const bool* mask);

    EXPORT void* vector_create_zeros(const uint size);
    EXPORT void* vector_create_ones(const uint size);
    EXPORT void* vector_concat(const void* first, const void* second);
#ifdef __cplusplus
}
#endif