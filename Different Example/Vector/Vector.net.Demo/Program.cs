﻿using System;

namespace Vector.net.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (var vector = new Vector(3))
                {
                    Console.WriteLine(vector.IsArrayExtern);
                }

                Console.WriteLine();

                double[] arr = { 1, 2, 3 };
                using (var vector = new Vector(arr))
                {
                    Console.WriteLine(vector.IsArrayExtern);

                    vector[1] = 42;

                    double[] tmp = null;
                    for (int i = 0; i < 1_000; ++i)
                        tmp = new double[10_000];

                    vector[1] = 666;

                    Console.WriteLine(vector[0]);
                    Console.WriteLine(vector[1]);
                    Console.WriteLine(vector[2]);

                    bool[] mask = { true, false, true };
                    vector.Mask(mask);

                    Console.WriteLine(vector[0]);
                    Console.WriteLine(vector[1]);
                    Console.WriteLine(vector[2]);
                }

                Console.WriteLine();

                using (var vector = new Vector(arr, false))
                {
                    Console.WriteLine(vector.IsArrayExtern);

                    vector[1] = 42;

                    double[] tmp = null;
                    for (int i = 0; i < 1_000; ++i)
                        tmp = new double[10_000];

                    vector[1] = 666;

                    Console.WriteLine(vector[0]);
                    Console.WriteLine(vector[1]);
                    Console.WriteLine(vector[2]);
                }

                using (var vector1 = new Vector(2))
                using (var vector2 = new Vector(3))
                    vector1.DotProduct(vector2);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }
    }
}