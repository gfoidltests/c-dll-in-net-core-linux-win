#include "Vector.h"

int main()
{
    Vector vec(3);

    Vector* v1 = new Vector(2);
    delete v1;

    auto v2 = Vector::Ones(3);
    delete v2;

    double val = vec[1];
    vec[1] = 2;
    val = vec[1];
}