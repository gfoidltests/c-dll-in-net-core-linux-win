#include "Vector.h"
//-----------------------------------------------------------------------------
Vector::Vector(const uint size)
{
    _size = size;
    _array = new double[size];
}
//-----------------------------------------------------------------------------
Vector::Vector(double* arr, const uint size)
{
    _size = size;
    _array = arr;
    _arrayExtern = true;
}
//-----------------------------------------------------------------------------
Vector* Vector::Zeros(const uint size)
{
    return Vector::Values(0, size);
}
//-----------------------------------------------------------------------------
Vector* Vector::Ones(const uint size)
{
    return Vector::Values(1, size);
}
//-----------------------------------------------------------------------------
Vector* Vector::Concat(const Vector& first, const Vector& second)
{
    Vector* vector = new Vector(first._size + second._size);
    double* array = vector->_array;

    for (uint i = 0; i < first._size; ++i)
        *array++ = first._array[i];

    for (uint i = 0; i < second._size; ++i)
        *array++ = second._array[i];

    return vector;
}
//-----------------------------------------------------------------------------
Vector* Vector::Values(const int value, const uint size)
{
    Vector* vec = new Vector(size);

    for (uint i = 0; i < size; ++i)
        vec->_array[i] = value;

    return vec;
}
//-----------------------------------------------------------------------------
Vector::~Vector()
{
    if (!_arrayExtern)
        delete[] _array;
}
//-----------------------------------------------------------------------------
double& Vector::operator[](const uint i)
{
    if (i >= _size)
        throw std::invalid_argument("Index out of range");

    return _array[i];
}
//-----------------------------------------------------------------------------
double Vector::DotProduct(const Vector& other)
{
    if (_size != other._size)
        throw std::invalid_argument("Size must be the same");

    double sum = 0;

    for (uint i = 0; i < _size; ++i)
        sum += _array[i] * other._array[i];

    return sum;
}
//-----------------------------------------------------------------------------
double Vector::Sum()
{
    double sum = 0;

    for (uint i = 0; i < _size; ++i)
        sum += _array[i];

    return sum;
}
//-----------------------------------------------------------------------------
bool Vector::IsOrthogonalTo(const Vector& other)
{
    double dot = this->DotProduct(other);

    return abs(dot) < 1e-6;
}
//-----------------------------------------------------------------------------
void Vector::Add(const double value)
{
    for (uint i = 0; i < _size; ++i)
        _array[i] += value;
}
//-----------------------------------------------------------------------------
void Vector::Add(const Vector& other)
{
    if (_size != other._size)
        throw std::invalid_argument("Size must be the same");

    for (uint i = 0; i < _size; ++i)
        _array[i] += other._array[i];
}
//-----------------------------------------------------------------------------
void Vector::Mask(const bool* mask)
{
    for (uint i = 0; i < _size; ++i)
        if (mask[i])
            _array[i] = 0;
}