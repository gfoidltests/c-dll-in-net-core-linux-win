#pragma once

#include <stdexcept>

typedef unsigned int uint;

class Vector
{
public:
    Vector(const uint size);
    Vector(double* arr, const uint size);
    ~Vector();

    uint getSize() { return _size; }
    bool getIsArrayExtern() { return _arrayExtern; }

    double& operator[](const uint i);
    double DotProduct(const Vector& other);
    double Sum();
    bool IsOrthogonalTo(const Vector& other);

    void Add(const double value);
    void Add(const Vector& other);
    void Mask(const bool* mask);

    static Vector* Zeros(const uint size);
    static Vector* Ones(const uint size);
    static Vector* Concat(const Vector& first, const Vector& second);

private:
    bool _arrayExtern = false;
    uint _size;
    double* _array;

    static Vector* Values(const int value, const uint size);
};