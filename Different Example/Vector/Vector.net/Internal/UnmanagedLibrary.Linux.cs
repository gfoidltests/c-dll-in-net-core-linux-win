﻿using System;
using System.Runtime.InteropServices;

namespace Vector.net.Internal
{
    partial class UnmanagedLibrary
    {
        private static class Linux
        {
            /*
             * Possible values are when the *.so resides next the app
             * libdl
             * libdl.so
             */
            private const string LIB = "libdl";
            //-----------------------------------------------------------------
            [DllImport(LIB)]
            public static extern IntPtr dlopen(string libraryPath, int flags);
            //-----------------------------------------------------------------
            [DllImport(LIB)]
            public static extern IntPtr dlsym(IntPtr handle, string symbol);
            //---------------------------------------------------------------------
            [DllImport(LIB)]
            public static extern void dlclose(IntPtr handle);
            //-----------------------------------------------------------------
            [DllImport(LIB, CharSet = CharSet.Ansi)]
            [return: MarshalAs(UnmanagedType.LPStr)]
            public static extern string dlerror();
        }
    }
}