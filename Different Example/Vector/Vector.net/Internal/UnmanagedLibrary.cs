﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Vector.net.Internal
{
    internal sealed partial class UnmanagedLibrary : IDisposable
    {
        private readonly string _libraryName;
        private IntPtr _handle;
        //---------------------------------------------------------------------
        /// <summary>
        /// When <c>false</c> the library gets unloaded when the AppDomain is
        /// unloaded.
        /// </summary>
        public static bool UnloadLibraryOnDispose { get; set; } = false;
        //---------------------------------------------------------------------
        public UnmanagedLibrary(string libraryName)
        {
            _libraryName = RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ?
                $"{libraryName}.dll" :
                $"./lib{libraryName}.so";   // Cf. http://man7.org/linux/man-pages/man3/dlopen.3.html for /

            _handle = this.LoadLibrary();

            if (_handle == IntPtr.Zero)
            {
                string error = GetError();
                throw new IOException($"Error in loading library {_libraryName}: {error}");
            }
        }
        //---------------------------------------------------------------------
        public T GetNativeMethodDelegate<T>(string methodName) where T : class
        {
            this.ThrowIfDisposed();

            IntPtr ptr = this.LoadSymbol(methodName);

            if (ptr == IntPtr.Zero)
                throw new MissingMethodException($"The native method '{methodName}' does not exist in '{_libraryName}'");

            return Marshal.GetDelegateForFunctionPointer<T>(ptr);
        }
        //---------------------------------------------------------------------
        private IntPtr LoadLibrary()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return Windows.LoadLibrary(Path.Combine(AppContext.BaseDirectory, _libraryName));
            else
            {
                const int RTLD_LAZY = 1;
                const int RTLD_GLOBAL = 8;

                return Linux.dlopen(_libraryName, RTLD_GLOBAL + RTLD_LAZY);
            }
        }
        //---------------------------------------------------------------------
        private IntPtr LoadSymbol(string symbolName)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return Windows.GetProcAddress(_handle, symbolName);
            else
                return Linux.dlsym(_handle, symbolName);
        }
        //---------------------------------------------------------------------
        private void FreeLibraray()
        {
            if (_handle == IntPtr.Zero) return;

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                Windows.FreeLibrary(_handle);
            else
                Linux.dlclose(_handle);

            _handle = IntPtr.Zero;
        }
        //---------------------------------------------------------------------
        private static string GetError()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return Windows.GetError();
            else
                return Linux.dlerror();
        }
        //---------------------------------------------------------------------
        #region IDisposable Members
        private bool _isDisposed = false;
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        private void ThrowIfDisposed()
        {
            if (_isDisposed) throw new ObjectDisposedException(this.ToString());
        }
        //---------------------------------------------------------------------
        private void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // nothing to do
                }

                // Otherwise no two person can be disposed
                if (UnloadLibraryOnDispose)
                    this.FreeLibraray();

                _isDisposed = true;
            }
        }
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        //---------------------------------------------------------------------
        ~UnmanagedLibrary()
        {
            this.Dispose(false);
        }
        #endregion
    }
}