﻿using System;

namespace Vector.net.Internal
{
    internal class NativeMethods
    {
        public readonly Delegates.vector_create            vector_create;
        public readonly Delegates.vector_create_with_array vector_create_with_array;
        public readonly Delegates.vector_destroy           vector_destroy;
        public readonly Delegates.vector_get_size          vector_get_size;
        public readonly Delegates.vector_is_array_extern   vector_is_array_extern;
        public readonly Delegates.vector_index_get         vector_index_get;
        public readonly Delegates.vector_index_set         vector_index_set;
        public readonly Delegates.vector_dotproduct        vector_dotproduct;
        public readonly Delegates.vector_sum               vector_sum;
        public readonly Delegates.vector_is_orthogonalto   vector_is_orthogonalto;
        public readonly Delegates.vector_add_scalar        vector_add_scalar;
        public readonly Delegates.vector_add_vector        vector_add_vector;
        public readonly Delegates.vector_mask              vector_mask;
        public readonly Delegates.vector_create_zeros      vector_create_zeros;
        public readonly Delegates.vector_create_ones       vector_create_ones;
        public readonly Delegates.vector_concat            vector_concat;
        //---------------------------------------------------------------------
        public NativeMethods()
        {
            var library = new UnmanagedLibrary("Vector.Library");

            vector_create            = GetMethodDelegate<Delegates.vector_create>();
            vector_create_with_array = GetMethodDelegate<Delegates.vector_create_with_array>();
            vector_destroy           = GetMethodDelegate<Delegates.vector_destroy>();
            vector_get_size          = GetMethodDelegate<Delegates.vector_get_size>();
            vector_is_array_extern   = GetMethodDelegate<Delegates.vector_is_array_extern>();
            vector_index_get         = GetMethodDelegate<Delegates.vector_index_get>();
            vector_index_set         = GetMethodDelegate<Delegates.vector_index_set>();
            vector_dotproduct        = GetMethodDelegate<Delegates.vector_dotproduct>();
            vector_sum               = GetMethodDelegate<Delegates.vector_sum>();
            vector_is_orthogonalto   = GetMethodDelegate<Delegates.vector_is_orthogonalto>();
            vector_add_scalar        = GetMethodDelegate<Delegates.vector_add_scalar>();
            vector_add_vector        = GetMethodDelegate<Delegates.vector_add_vector>();
            vector_mask              = GetMethodDelegate<Delegates.vector_mask>();
            vector_create_zeros      = GetMethodDelegate<Delegates.vector_create_zeros>();
            vector_create_ones       = GetMethodDelegate<Delegates.vector_create_ones>();
            vector_concat            = GetMethodDelegate<Delegates.vector_concat>();
            //-----------------------------------------------------------------
            T GetMethodDelegate<T>() where T : class
            {
                return library.GetNativeMethodDelegate<T>(typeof(T).Name);
            }
        }
        //---------------------------------------------------------------------
        public static class Delegates
        {
            public delegate IntPtr vector_create(uint size);
            public delegate IntPtr vector_create_with_array(IntPtr arr, uint size);
            public delegate void vector_destroy(IntPtr vector);

            public delegate uint vector_get_size(IntPtr vector);
            public delegate bool vector_is_array_extern(IntPtr vector);

            public delegate double vector_index_get(IntPtr vector, uint i);
            public delegate void vector_index_set(IntPtr vector, uint i, double value);
            public delegate double vector_dotproduct(IntPtr vec1, IntPtr vec2);
            public delegate double vector_sum(IntPtr vector);
            public delegate bool vector_is_orthogonalto(IntPtr vec1, IntPtr vec2);

            public delegate void vector_add_scalar(IntPtr vector, double scalar);
            public delegate void vector_add_vector(IntPtr vector, IntPtr other);
            public unsafe delegate void vector_mask(IntPtr vector, bool* mask);

            public delegate IntPtr vector_create_zeros(uint size);
            public delegate IntPtr vector_create_ones(uint size);
            public delegate IntPtr vector_concat(IntPtr first, IntPtr second);
        }
    }
}