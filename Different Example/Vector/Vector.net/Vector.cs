﻿using System;
using System.Runtime.InteropServices;
using Vector.net.Internal;

namespace Vector.net
{
    public sealed class Vector : IDisposable
    {
        private static Lazy<NativeMethods> _nativeMethods = new Lazy<NativeMethods>();
        private IntPtr _vector;
        private GCHandle _arrayHandle;
        //---------------------------------------------------------------------
        private Vector(IntPtr vector) => _vector = vector;
        //---------------------------------------------------------------------
        public Vector(int size)
        {
            _vector = _nativeMethods.Value.vector_create((uint)size);
        }
        //---------------------------------------------------------------------
        public Vector(double[] arr, bool pinArray = true)
        {
            if (pinArray)
            {
                /*
                 * Das double[] könnte direkt übergeben werden und es schaut passend aus.
                 * Bis zur ersten GC, denn dort wird das double[] verschoben und schon hat
                 * C++ keine Zugriff mehr darauf.
                 */
                _arrayHandle = GCHandle.Alloc(arr, GCHandleType.Pinned);
                _vector = _nativeMethods.Value.vector_create_with_array(_arrayHandle.AddrOfPinnedObject(), (uint)arr.Length);
            }
            else
            {
                _vector = _nativeMethods.Value.vector_create((uint)arr.Length);

                for (int i = 0; i < arr.Length; ++i)
                    this[i] = arr[i];
            }
        }
        //---------------------------------------------------------------------
        public int Size
        {
            get => (int)_nativeMethods.Value.vector_get_size(_vector);
        }
        //---------------------------------------------------------------------
        public bool IsArrayExtern
        {
            get => _nativeMethods.Value.vector_is_array_extern(_vector);
        }
        //---------------------------------------------------------------------
        public double this[int i]
        {
            get
            {
                this.ThrowIfDisposed();
                return _nativeMethods.Value.vector_index_get(_vector, (uint)i);
            }
            set
            {
                this.ThrowIfDisposed();
                _nativeMethods.Value.vector_index_set(_vector, (uint)i, value);
            }
        }
        //---------------------------------------------------------------------
        public double DotProduct(Vector other, bool checkSize = true)
        {
            if (checkSize && this.Size != other.Size)
                throw new ArgumentException("Size of the vectors must be the same");

            this.ThrowIfDisposed();
            return _nativeMethods.Value.vector_dotproduct(_vector, other._vector);
        }
        //---------------------------------------------------------------------
        public double Sum()
        {
            this.ThrowIfDisposed();
            return _nativeMethods.Value.vector_sum(_vector);
        }
        //---------------------------------------------------------------------
        public bool IsOrthogonalTo(Vector other, bool checkSize = true)
        {
            if (checkSize && this.Size != other.Size)
                throw new ArgumentException("Size of the vectors must be the same");

            this.ThrowIfDisposed();
            return _nativeMethods.Value.vector_is_orthogonalto(_vector, other._vector);
        }
        //---------------------------------------------------------------------
        public void Add(double scalar)
        {
            this.ThrowIfDisposed();
            _nativeMethods.Value.vector_add_scalar(_vector, scalar);
        }
        //---------------------------------------------------------------------
        public void Add(Vector other)
        {
            this.ThrowIfDisposed();
            _nativeMethods.Value.vector_add_vector(_vector, other._vector);
        }
        //---------------------------------------------------------------------
        public unsafe void Mask(bool[] mask)
        {
            this.ThrowIfDisposed();

            fixed (bool* ptr = mask)
                _nativeMethods.Value.vector_mask(_vector, ptr);
        }
        //---------------------------------------------------------------------
        public static Vector CreateZeros(int size)
        {
            IntPtr vector = _nativeMethods.Value.vector_create_zeros((uint)size);
            return new Vector(vector);
        }
        //---------------------------------------------------------------------
        public static Vector CreateOnes(int size)
        {
            IntPtr vector = _nativeMethods.Value.vector_create_ones((uint)size);
            return new Vector(vector);
        }
        //---------------------------------------------------------------------
        public static Vector Concat(Vector first, Vector second)
        {
            IntPtr vector = _nativeMethods.Value.vector_concat(first._vector, second._vector);
            return new Vector(vector);
        }
        //---------------------------------------------------------------------
        #region IDisposable Members
        private bool _isDisposed = false;
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        private void ThrowIfDisposed()
        {
            if (_isDisposed) throw new ObjectDisposedException(this.ToString());
        }
        //---------------------------------------------------------------------
        private void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (_vector != IntPtr.Zero && _nativeMethods.IsValueCreated)
                {
                    _nativeMethods.Value.vector_destroy(_vector);
                    _vector = IntPtr.Zero;
                }

                if (disposing)
                {
                    if (_arrayHandle.IsAllocated)
                        _arrayHandle.Free();
                }

                _isDisposed = true;
            }
        }
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        //---------------------------------------------------------------------
        ~Vector()
        {
            this.Dispose(false);
        }
        #endregion
    }
}