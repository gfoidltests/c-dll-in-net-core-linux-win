﻿using NUnit.Framework;

namespace Vector.net.Tests.VectorTests
{
    [TestFixture]
    public class CreateOnes
    {
        [Test]
        public void Size_given___correct_Vector_created()
        {
            var sut = Vector.CreateOnes(3);

            Assert.AreEqual(1, sut[0]);
            Assert.AreEqual(1, sut[1]);
            Assert.AreEqual(1, sut[2]);
        }
    }
}