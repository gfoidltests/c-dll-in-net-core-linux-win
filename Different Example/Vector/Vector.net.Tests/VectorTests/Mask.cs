﻿using NUnit.Framework;

namespace Vector.net.Tests.VectorTests
{
    [TestFixture]
    public class Mask
    {
        [Test]
        public void Mask_given___correct_masking_of_Vector()
        {
            double[] array = { 1, 2, 3 };
            bool[] mask = { true, false, true };

            var sut = new Vector(array);

            sut.Mask(mask);

            Assert.AreEqual(0, sut[0], 1e-6);
            Assert.AreEqual(2, sut[1], 1e-6);
            Assert.AreEqual(0, sut[2], 1e-6);
        }
    }
}