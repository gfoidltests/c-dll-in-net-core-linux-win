﻿using NUnit.Framework;

namespace Vector.net.Tests.VectorTests
{
    [TestFixture]
    public class Concat
    {
        [Test]
        public void Vectors_given___correct_resulting_vector()
        {
            double[] arr1 = { 1, 2, 3 };
            double[] arr2 = { 4, 5 };

            var v1 = new Vector(arr1);
            var v2 = new Vector(arr2);

            Vector actual = Vector.Concat(v1, v2);

            Assert.AreEqual(5, actual.Size);
            Assert.AreEqual(1, actual[0], 1e-6);
            Assert.AreEqual(2, actual[1], 1e-6);
            Assert.AreEqual(3, actual[2], 1e-6);
            Assert.AreEqual(4, actual[3], 1e-6);
            Assert.AreEqual(5, actual[4], 1e-6);
        }
    }
}