﻿using NUnit.Framework;

namespace Vector.net.Tests.VectorTests
{
    [TestFixture]
    public class Add
    {
        [Test]
        public void Scalar_given___value_added_to_elements()
        {
            double[] arr = { 1, 2, 3 };

            var sut = new Vector(arr);

            sut.Add(1.1);

            Assert.AreEqual(2.1, sut[0], 1e-6);
            Assert.AreEqual(3.1, sut[1], 1e-6);
            Assert.AreEqual(4.1, sut[2], 1e-6);
        }
    }
}