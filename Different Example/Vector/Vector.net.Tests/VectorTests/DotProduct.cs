﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace Vector.net.Tests.VectorTests
{
    [TestFixture]
    public class DotProduct
    {
        [Test]
        public void Vectors_given___correct_result()
        {
            double[] arr1 = { 1, 2, 3 };
            double[] arr2 = { 4, 5, 6 };

            var vec1 = new Vector(arr1);
            var vec2 = new Vector(arr2);

            double actual = vec1.DotProduct(vec2);

            Assert.AreEqual(32, actual, 1e-6);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Vector_size_do_not_match___Throws_Argument_Exception()
        {
            var vec1 = new Vector(2);
            var vec2 = new Vector(3);

            Assert.Throws<ArgumentException>(() =>
            {
                double actual = vec1.DotProduct(vec2);
            });
        }
    }
}