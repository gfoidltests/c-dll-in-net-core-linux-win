﻿using NUnit.Framework;

namespace Vector.net.Tests.VectorTests
{
    [TestFixture]
    public class Ctor
    {
        [Test]
        public void Size_given___OK()
        {
            var sut = new Vector(3);

            Assert.AreEqual(3, sut.Size);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Array_given___OK()
        {
            double[] arr = { 1, 2, 3 };

            var sut = new Vector(arr);

            Assert.AreEqual(1, sut[0], 1e-6);
            Assert.AreEqual(2, sut[1], 1e-6);
            Assert.AreEqual(3, sut[2], 1e-6);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Array_given_and_GC_happens___OK([Values(true, false)] bool pinArray)
        {
            double[] arr = { 1, 2, 3 };

            var sut = new Vector(arr, pinArray);

            double[] tmp = null;
            for (int i = 0; i < 1_000; ++i)
                tmp = new double[10_000];

            Assert.AreEqual(1, sut[0], 1e-6);
            Assert.AreEqual(2, sut[1], 1e-6);
            Assert.AreEqual(3, sut[2], 1e-6);
        }
    }
}