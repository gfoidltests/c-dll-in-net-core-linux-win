﻿using System;
using NUnit.Framework;

namespace Vector.net.Tests.VectorTests
{
    [TestFixture]
    public class IsOrthogonalTo
    {
        [Test]
        public void Orthogonal_vectors_given___true()
        {
            double[] arr1 = { 2, 1 };
            double[] arr2 = { 1, -2 };

            var v1 = new Vector(arr1);
            var v2 = new Vector(arr2);

            bool actual = v1.IsOrthogonalTo(v2);

            Assert.IsTrue(actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Vectors_given___false()
        {
            double[] arr1 = { 2, 1 };
            double[] arr2 = { 1, 2 };

            var v1 = new Vector(arr1);
            var v2 = new Vector(arr2);

            bool actual = v1.IsOrthogonalTo(v2);

            Assert.IsFalse(actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Vector_sizes_do_not_match___Throws_Index_Exception()
        {
            var v1 = new Vector(2);
            var v2 = new Vector(3);

            Assert.Throws<ArgumentException>(() =>
            {
                bool actual = v1.IsOrthogonalTo(v2);
            });
        }
    }
}