﻿using System;
using MyCHostnetLoad.Internal;

namespace MyCHostnetLoad
{
    public sealed class Person : IDisposable
    {
        private static NativeMethods _nativeMethods;
        private IntPtr               _personHandle;
        //---------------------------------------------------------------------
        public Person(string name)
        {
            EnsureNativeMethodsSetup();

            _personHandle = _nativeMethods.person_create(name);
            //-----------------------------------------------------------------
            void EnsureNativeMethodsSetup()
            {
                if (_nativeMethods != null) return;

                _nativeMethods = new NativeMethods();
            }
        }
        //---------------------------------------------------------------------
        public void Info()
        {
            this.ThrowIfDisposed();
            _nativeMethods.person_info(_personHandle);
        }
        //---------------------------------------------------------------------
        public void Love(Person other)
        {
            this.ThrowIfDisposed();
            _nativeMethods.person_love(_personHandle, other._personHandle);
        }
        //---------------------------------------------------------------------
        #region IDisposable Members
        private bool _isDisposed = false;
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        private void ThrowIfDisposed()
        {
            if (_isDisposed) throw new ObjectDisposedException(this.ToString());
        }
        //---------------------------------------------------------------------
        private void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (_personHandle != IntPtr.Zero)
                {
                    _nativeMethods.person_dispose(_personHandle);
                    _personHandle = IntPtr.Zero;
                }

                if (disposing)
                {
                    // must be called after freeing of the person handle
                }

                _isDisposed = true;
            }
        }
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        //---------------------------------------------------------------------
        ~Person()
        {
            this.Dispose(false);
        }
        #endregion
    }
}