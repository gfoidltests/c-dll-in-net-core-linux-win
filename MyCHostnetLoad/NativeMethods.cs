﻿using System;
using System.Collections.Concurrent;

namespace MyCHostnetLoad.Internal
{
    internal class NativeMethods
    {
        private static ConcurrentDictionary<string, UnmanagedLibrary> _libraries = new ConcurrentDictionary<string, UnmanagedLibrary>();

        public readonly Delegates.person_create_delegate  person_create;
        public readonly Delegates.person_dispose_delegate person_dispose;
        public readonly Delegates.person_info_delegate    person_info;
        public readonly Delegates.person_love_delegate    person_love;
        public readonly Delegates.add_int                 add_int;
        public readonly Delegates.add_double              add_double;
        //---------------------------------------------------------------------
        public NativeMethods(string libraryName = "MyCLibrary")
        {
            UnmanagedLibrary library = GetLibrary(libraryName);

            person_create  = GetMethodDelegate<Delegates.person_create_delegate>(library);
            person_dispose = GetMethodDelegate<Delegates.person_dispose_delegate>(library);
            person_info    = GetMethodDelegate<Delegates.person_info_delegate>(library);
            person_love    = GetMethodDelegate<Delegates.person_love_delegate>(library);
            add_int        = GetMethodDelegate<Delegates.add_int>(library);
            add_double     = GetMethodDelegate<Delegates.add_double>(library);
        }
        //---------------------------------------------------------------------
        private static UnmanagedLibrary GetLibrary(string libraryName)
        {
            return _libraries.GetOrAdd(libraryName, l => new UnmanagedLibrary(l));
        }
        //---------------------------------------------------------------------
        private static T GetMethodDelegate<T>(UnmanagedLibrary library) where T : class
        {
            string methodName = RemoveSuffix(typeof(T).Name, "_delegate");
            return library.GetNativeMethodDelegate<T>(methodName);
        }
        //---------------------------------------------------------------------
        private static string RemoveSuffix(string str, string suffix)
        {
            if (!str.EndsWith(suffix)) return str;

            return str.Substring(0, str.Length - suffix.Length);
        }
        //---------------------------------------------------------------------
        public static class Delegates
        {
            public delegate IntPtr person_create_delegate(string name);
            public delegate void person_dispose_delegate(IntPtr person);
            public delegate void person_info_delegate(IntPtr person);
            public delegate void person_love_delegate(IntPtr person, IntPtr other);
            public delegate int add_int(int a, int b);
            public delegate double add_double(double a, double b);
        }
    }
}