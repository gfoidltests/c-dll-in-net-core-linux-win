//#define CPP

#include "PersonCaller.h"

int main()
{
#ifdef CPP
    Person p1("Adam");
    Person p2("Eva");

    p1.Info();
    p1.Love(p2);
#else
    void* p1;
    void* p2;

    p1 = person_create("Adam");
    p2 = person_create("Eva");

    person_info(p1);
    person_love(p1, p2);
#endif
}