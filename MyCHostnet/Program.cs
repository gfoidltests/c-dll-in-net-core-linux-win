﻿using System;
using MyCHostnet.Internal;

namespace MyCHostnet
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var p1 = new Person("Adam"))
            using (var p2 = new Person("Eva"))
            {
                p1.Info();
                p1.Love(p2);
            }

            Console.WriteLine();
            Console.WriteLine($"3 + 4 = {NativeApi.add(3, 4)}");

            double a = 2.1;
            double b = 8.4;
            Console.WriteLine($"{a} + {b} = {NativeApi.add(2.1, 8.4)}");
        }
    }
}