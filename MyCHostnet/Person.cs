﻿using System;
using MyCHostnet.Internal;

namespace MyCHostnet
{
    public class Person : IDisposable
    {
        private readonly IntPtr _person;
        //---------------------------------------------------------------------
        public Person(string name) => _person = NativeApi.person_create(name);
        //---------------------------------------------------------------------
        public void Info() => NativeApi.person_info(_person);
        public void Love(Person other) => NativeApi.person_love(_person, other._person);
        //---------------------------------------------------------------------
        #region IDisposable Members
        private bool _isDisposed = false;
        //---------------------------------------------------------------------
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // managed ressources
                }

                // unmanaged ressources
                NativeApi.person_dispose(_person);

                _isDisposed = true;
            }
        }
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        //---------------------------------------------------------------------
        ~Person()
        {
            this.Dispose(false);
        }
        #endregion
    }
}