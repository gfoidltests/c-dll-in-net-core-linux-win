﻿using System;
using System.Runtime.InteropServices;

namespace MyCHostnet.Internal
{
    internal static partial class NativeApi
    {
        public static IntPtr person_create(string name)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return Windows.person_create(name);
            else
                return Linux.person_create(name);
        }
        //---------------------------------------------------------------------
        public static void person_dispose(IntPtr person)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                Windows.person_dispose(person);
            else
                Linux.person_dispose(person);
        }
        //---------------------------------------------------------------------
        public static void person_info(IntPtr person)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                Windows.person_info(person);
            else
                Linux.person_info(person);
        }
        //---------------------------------------------------------------------
        public static void person_love(IntPtr person, IntPtr other)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                Windows.person_love(person, other);
            else
                Linux.person_love(person, other);
        }
        //---------------------------------------------------------------------
        public static int add(int a, int b)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return Windows.add_int(a, b);
            else
                return Linux.add_int(a, b);
        }
        //---------------------------------------------------------------------
        public static double add(double a, double b)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return Windows.add_double(a, b);
            else
                return Linux.add_double(a, b);
        }
    }
}