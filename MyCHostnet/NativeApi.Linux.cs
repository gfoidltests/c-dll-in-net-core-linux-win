﻿using System;
using System.Runtime.InteropServices;

namespace MyCHostnet.Internal
{
    partial class NativeApi
    {
        private static class Linux
        {
            /*
             * Possible values are when the *.so resides next the app:
             * libMyCLibrary
             * libMyCLibrary.so
             * ./libMyCLibrary
             * ./libMyCLibrary.so
             */
            private const string LIB = "libMyCLibrary";

            [DllImport(LIB)]
            public static extern IntPtr person_create(string name);

            [DllImport(LIB)]
            public static extern void person_dispose(IntPtr person);

            [DllImport(LIB)]
            public static extern void person_info(IntPtr person);

            [DllImport(LIB)]
            public static extern void person_love(IntPtr person, IntPtr other);

            [DllImport(LIB)]
            public static extern int add_int(int a, int b);

            [DllImport(LIB)]
            public static extern double add_double(double a, double b);
        }
    }
}